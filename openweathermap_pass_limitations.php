# openweathermap does not allow more than 20 city 
# id's per call, and free API key allows 60 calls 
# per minute. One city is one API call:
# https://openweathermap.org/current#severalid
# 
# mod will limit locations to 20 per API call, 
# and make a pause between requests.
#
# in server/lib/openweathermap.class.php replace function:


    private function updateCurrentByGroupIds($ids){

        $chunks = array();
        $idx = 0;

        foreach ($ids as $id){
            if (!isset($chunks[$idx])){
                $chunks[$idx] = $id;
            }else{
                if (strlen($chunks[$idx]) + strlen(','.$id) > 1000){
                    $idx++;
                    $chunks[$idx] = $id;
                }else{
                    $chunks[$idx] .= ','.$id;
                }
            }
        }

      $bytwnty=array_chunk($ids, 20);
      foreach ($bytwnty as $twnty){

        $url = 'http://api.openweathermap.org/data/2.5/group?id='.implode(',', $twnty);

        $content = file_get_contents(
            $url,
            false,
            stream_context_create($this->context_params)
        );

        $content = json_decode($content, true);

        if ($content && !empty($content['list'])){
            foreach ($content['list'] as $weather){
                $weather = $this->normalizeWeatherData($weather);
                if (!empty($weather)){
                    $this->setCurrentCache($weather);
                }
            }
        }
      sleep(16);
      }
    }

